# Image Style Preview

Provides previews of the various image styles in the site.

1. Enable the module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.
2. Ensure that Standalone media URL is enabled at/admin/config/media/media-settings
3. View any image media entity page, e.g. /media/123 - each image style, plus the original image, is displayed as a collapsible details/summary element

To select which image styles are shown as expanded by default, go to /admin/config/media/image-styles/imagestyles
